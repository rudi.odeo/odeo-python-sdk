Usage
=====

Installation
------------

To use Odeo Python SDK, first install it using pip:

.. code-block:: console
    
    pip install odeo-python-sdk